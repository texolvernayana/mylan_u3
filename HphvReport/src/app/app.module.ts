import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module';
import { AccordionModule } from 'primeng/accordion';
import { TableModule } from 'primeng/table';
import { ChartModule } from 'primeng/chart';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { TopNavigatorComponent } from './common/top-navigator/top-navigator.component';
import { BottomNavigatorComponent } from './common/bottom-navigator/bottom-navigator.component';
import { LoginComponent } from './login/login.component';

import { ParametersComponent } from './parameters/parameters.component';
import { SearchreunidsComponent } from './parameters/searchreunids/searchreunids.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    AccordionModule,
    TableModule,
    ChartModule,
    HttpClientModule,
    OwlDateTimeModule, OwlNativeDateTimeModule
  ],
  declarations: [
    AppComponent,
    TopNavigatorComponent,
    BottomNavigatorComponent,
    LoginComponent,
    SearchreunidsComponent,
    ParametersComponent,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
