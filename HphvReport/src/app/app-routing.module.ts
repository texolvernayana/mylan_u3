import { NgModule, ViewChild } from '@angular/core';
//import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { AppComponent } from './app.component'
import { LoginComponent } from './login/login.component';
import { SearchreunidsComponent } from './parameters/searchreunids/searchreunids.component';
import { ParametersComponent } from './parameters/parameters.component';

export const routes: Routes = [
  { path: '', component: LoginComponent },
  //  { path: '', pathMatch: 'full', redirectTo: 'Login' },
  {path: 'parameter', component: ParametersComponent},
  { path: 'searchrunIdList', component: SearchreunidsComponent, },
  { loadChildren: '../app/parameters/parameter.module#ParametersModule', path: 'searchreunids' },
  { loadChildren: '../app/Report/testreport/testreport.module#TestreportModule', path: 'Report/TestReport' },
  { loadChildren: '../app/Report/rephphvprocess/rephphvprocess.module#RephphvprocessModule', path: 'Report/HphvReport' },
  { loadChildren: '../app/Report/reportstandardbowie/reportstandardbowie.module#ReportstandardbowieModule', path: 'Report/BowieReport' },
  { loadChildren: '../app/Report/reportvacuum/reportvacuum.module#ReportvacuumModule', path: 'Report/Vacuumreport' },
 ];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

// export const routingComponentList = [ParametersComponent]
