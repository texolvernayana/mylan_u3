import { Component, OnInit } from '@angular/core';
import { HphvapiService } from '../../rephphvprocess/hphvapi.service';
import { Istandard, standard } from 'src/app/Common/Models/Istandard';

@Component({
  selector: 'app-report-standarsetparameter',
  templateUrl: './standardstandarsetparameter.component.html',
  styleUrls: ['./standardstandarsetparameter.component.css']
})
export class StandardstandarsetparameterComponent implements OnInit {

  constructor(private api: HphvapiService) { 
    this.standardBatchdetails = new standard();
  }
  public standardBatchdetails: Istandard
  ngOnInit() {
    this.api.getStandardSetParameterDetails("4451").pipe().subscribe(s => this.standardBatchdetails = s);
  }

}
