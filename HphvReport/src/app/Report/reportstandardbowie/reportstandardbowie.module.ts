import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartModule } from 'primeng/chart';

import { ReportstandardbowieRoutingModule } from './reportstandardbowie-routing.module';
import { ReportstandardbowieComponent } from './reportstandardbowie.component';
import { BatchdataComponent } from '../batchdata/batchdata.component';
import { ReportheaderComponent } from '../reportheader/reportheader.component';
import { ReportfooterComponent } from '../reportfooter/reportfooter.component';
import { ReportdetailsComponent } from '../reportdetails/reportdetails.component';
import { TrendchartComponent } from '../trendchart/trendchart.component';
import { StandardstandarsetparameterComponent } from './standardstandarsetparameter/standardstandarsetparameter.component';

// HphvprocesssetparameterComponent
@NgModule({
  declarations: [ReportstandardbowieComponent, BatchdataComponent, ReportheaderComponent,
    ReportfooterComponent, ReportdetailsComponent, TrendchartComponent, StandardstandarsetparameterComponent], 

  imports: [
    CommonModule,
    ReportstandardbowieRoutingModule,
    ChartModule
  ]
})
export class ReportstandardbowieModule { }
