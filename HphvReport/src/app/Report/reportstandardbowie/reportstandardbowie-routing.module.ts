import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportstandardbowieComponent } from './reportstandardbowie.component';

const routes: Routes = [
  { path: '', component: ReportstandardbowieComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportstandardbowieRoutingModule { }
