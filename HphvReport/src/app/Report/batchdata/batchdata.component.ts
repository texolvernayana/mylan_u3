import { Component, OnInit, Input } from '@angular/core';
// import { IhphvProcess, hphvProcess } from 'src/app/Common/Models/IhphvProcess';
import { HphvapiService } from '../rephphvprocess/hphvapi.service';
import { Ibatch, batch } from 'src/app/Common/Models/Ibatch';

@Component({
  selector: 'app-report-batchdata',
  templateUrl: './batchdata.component.html',
  styleUrls: ['./batchdata.component.css']
})
export class BatchdataComponent implements OnInit {
  public name = "gg"
  public hphvBatchdetails: Ibatch;
 
  constructor(private api: HphvapiService) {
    this.hphvBatchdetails = new batch();
  }

  ngOnInit() {
    this.loadDataToGrid();
  }

  loadDataToGrid() {
    this.api.getBatchDetails_1("4451").subscribe(s => {
      this.hphvBatchdetails = s;
    }, error => console.log(error));
  }

}
