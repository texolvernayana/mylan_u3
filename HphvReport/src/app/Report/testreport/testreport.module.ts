import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestreportRoutingModule } from './testreport-routing.module';
import { TestreportComponent } from './testreport.component';
import { BatchdataComponent } from '../batchdata/batchdata.component'
import { ReportheaderComponent } from '../reportheader/reportheader.component'
import { HphvprocesssetparameterComponent } from '../rephphvprocess/hphvprocesssetparameter/hphvprocesssetparameter.component';
import { ReportfooterComponent } from '../reportfooter/reportfooter.component';

@NgModule({
  declarations: [TestreportComponent, BatchdataComponent, ReportheaderComponent, HphvprocesssetparameterComponent, ReportfooterComponent],
  imports: [
    CommonModule,
    TestreportRoutingModule
  ]
})
export class TestreportModule { }
