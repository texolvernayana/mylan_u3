import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportvacuumComponent } from './reportvacuum.component';

const routes: Routes = [
  { path: '', component: ReportvacuumComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportvacuumRoutingModule { }
