import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartModule } from 'primeng/chart';

import { ReportvacuumRoutingModule } from './reportvacuum-routing.module';
import { ReportvacuumComponent } from './reportvacuum.component';
import { BatchdataComponent } from '../batchdata/batchdata.component';
import { ReportheaderComponent } from '../reportheader/reportheader.component';
import { ReportfooterComponent } from '../reportfooter/reportfooter.component';
import { ReportdetailsComponent } from '../reportdetails/reportdetails.component';
import { TrendchartComponent } from '../trendchart/trendchart.component';
import { VacuumsetparameterComponent } from './vacuumsetparameter/vacuumsetparameter.component';

@NgModule({
  declarations: [ReportvacuumComponent, BatchdataComponent, ReportheaderComponent, VacuumsetparameterComponent
    , ReportfooterComponent, ReportdetailsComponent, TrendchartComponent],
  imports: [
    CommonModule,
    ReportvacuumRoutingModule,
    ChartModule
  ]
})
export class ReportvacuumModule { }
