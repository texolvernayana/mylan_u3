import { Component, OnInit } from '@angular/core';
import { Ivaccum, vaccum } from 'src/app/Common/Models/Ivacuum';
import { HphvapiService } from '../../rephphvprocess/hphvapi.service';

@Component({
  selector: 'app-repotr-vacuumsetparameter',
  templateUrl: './vacuumsetparameter.component.html',
  styleUrls: ['./vacuumsetparameter.component.css']
})
export class VacuumsetparameterComponent implements OnInit {

  public vacuumParameterdetails: Ivaccum;

  constructor(private api: HphvapiService) {
    this.vacuumParameterdetails = new vaccum();
  }

  ngOnInit() {
    this.api.getVacuumSetParameterDetails("4451").pipe().subscribe(s => this.vacuumParameterdetails = s);
  }

}
