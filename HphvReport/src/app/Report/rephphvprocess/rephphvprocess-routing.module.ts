import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import{RephphvprocessComponent} from './rephphvprocess.component'

const routes: Routes = [
  { path: '', component: RephphvprocessComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RephphvprocessRoutingModule { }
