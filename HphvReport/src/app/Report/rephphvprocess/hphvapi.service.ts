import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IhphvProcess } from '../../Common/Models/IhphvProcess';
// import { Observable, pipe } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
//import { Observable } from 'rxjs/Observable';

import { iRunDetails } from 'src/app/Common/Models/IrunDetails';
import { Ibatch } from 'src/app/Common/Models/Ibatch';
import { Ivaccum } from 'src/app/Common/Models/Ivacuum';
@Injectable({
  providedIn: 'root'
})
export class HphvapiService {
  private _url: string = "http://localhost:4201/api/hphvHederDetails";
  constructor(private http: HttpClient) { }
  
  getBatchDetails_1(batchNo: string): Observable<Ibatch> {
    return this.http.get<Ibatch>(this._url + "/reportHeader?BatchNo=4451")
    // return this.http.get(this._url + "/reportHeader?BatchNo=4451", {}).map(res => <Product[]>res.json());
  }
  
  //---Set Parameter start---///
  getHVBatchDetails(batchNo: string): Observable<IhphvProcess> {
    return this.http.get<IhphvProcess>(this._url + "/reportHeader?BatchNo=4451")
  }
  getStandardSetParameterDetails(batchNo: string): Observable<IhphvProcess> {
    return this.http.get<IhphvProcess>(this._url + "/reportHeader?BatchNo=4451")
  }
  getVacuumSetParameterDetails(batchNo: string): Observable<Ivaccum> {
    return this.http.get<Ivaccum>(this._url + "/reportHeader?BatchNo=4451")
  }
  //---Set Parameter End---///
  getProducts(): Observable<IhphvProcess[]> {
    return this.http.get(this._url + "/reportHeader?BatchNo=4451",{}).pipe(res => res[0]);
  }

  getRunDetails(batchNo: string): Observable<iRunDetails[]> {
    return this.http.get<iRunDetails[]>(this._url + "/reportRunDetails?BatchNo=4451")
  }

  getChartDetails(batchNo: string): Observable<any[]> {
    return this.http.get<any[]>(this._url + "/reportChartDetails?BatchNo=4451")
  }
  
}
