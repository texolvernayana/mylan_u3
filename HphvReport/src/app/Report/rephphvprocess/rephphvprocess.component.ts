import { Component, OnInit } from '@angular/core';
//import { DOCUMENT } from '@angular/common';
import jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';

import { TestBed } from '@angular/core/testing';

@Component({
  selector: 'app-rephphvprocess',
  templateUrl: './rephphvprocess.component.html',
  //templateUrl: './test.html',
  styleUrls: ['./rephphvprocess.component.css']
})
export class RephphvprocessComponent implements OnInit {

  constructor() {  }
  
   ngOnInit() {

   }

  converttopdfTest() {
    var data = document.getElementById('ReportTestView');
    html2canvas(data).then(canvas => {
      // Few necessary setting options  
      var imgWidth = 208;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save('HPHV.pdf'); // Generated PDF   
    });
  }
}
