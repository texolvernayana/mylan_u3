import { Component, OnInit, Input } from '@angular/core';
import { HphvapiService } from '../hphvapi.service';
import { IhphvProcess, hphvProcess } from 'src/app/Common/Models/IhphvProcess';

@Component({
  selector: 'app-report-hphvprocesssetparameter',
  templateUrl: './hphvprocesssetparameter.component.html',
  styleUrls: ['./hphvprocesssetparameter.component.css']
})
export class HphvprocesssetparameterComponent implements OnInit {

 public hphvBatchdetails: IhphvProcess;

  constructor(private api: HphvapiService) {
    this.hphvBatchdetails = new hphvProcess();
  }

  ngOnInit() {
    this.api.getHVBatchDetails("4451").pipe().subscribe(s => this.hphvBatchdetails = s);
  }
  
}
