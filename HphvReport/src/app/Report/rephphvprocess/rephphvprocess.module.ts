import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { TableModule } from 'primeng/table';
import { ChartModule } from 'primeng/chart';

import { RephphvprocessRoutingModule } from './rephphvprocess-routing.module';
import { RephphvprocessComponent } from './rephphvprocess.component';
import { BatchdataComponent } from '../batchdata/batchdata.component'
import { ReportheaderComponent } from '../reportheader/reportheader.component'
import { HphvprocesssetparameterComponent } from './hphvprocesssetparameter/hphvprocesssetparameter.component';
import { ReportfooterComponent } from '../reportfooter/reportfooter.component';
import { ReportdetailsComponent } from '../reportdetails/reportdetails.component';
import { TrendchartComponent } from '../trendchart/trendchart.component';
// import { VirtualScrollerModule } from 'primeng/virtualscroller';

@NgModule({
  declarations: [RephphvprocessComponent, BatchdataComponent, ReportheaderComponent, HphvprocesssetparameterComponent
               , ReportfooterComponent, ReportdetailsComponent, TrendchartComponent],
  imports: [
    CommonModule,
    RephphvprocessRoutingModule,
    //TableModule,
    ChartModule,
    //VirtualScrollerModule
  ]
})
export class RephphvprocessModule { }
