import { Component, OnInit } from '@angular/core';
import { HphvapiService } from '../rephphvprocess/hphvapi.service';

@Component({
  selector: 'app-trendchart',
  templateUrl: './trendchart.component.html',
  styleUrls: ['./trendchart.component.css']
})
export class TrendchartComponent implements OnInit {
  data: any;
  prData: any;
  options: any;
  prOptions: any;
  hphvChartDetails = [];
  constructor(private api: HphvapiService) { }

  ngOnInit() {
    this.api.getChartDetails("4451").subscribe((s: any[]) => { this.hphvChartDetails = s });

    // this.generateTempChartDatat(this.hphvChartDetails);
    // this.generatePresureChartDatat(this.hphvChartDetails);
  }
  fethcval()
  {
    this.generateTempChartDatat(this.hphvChartDetails);
    this.generatePresureChartDatat(this.hphvChartDetails);
  }
  generateTempChartDatat(d) {

    this.data = {
      labels: d[6].data,
      datasets: [
        {
          label: 'T1',
          data: d[0].data,
          borderColor: '#0101DF',
        },
        {
          label: 'T2',
          data: d[1].data,
          borderColor: '#FE2EF7',
        },
        {
          label: 'T3',
          data: d[2].data,
          borderColor: '#FF0000',
        },
        {
          label: 'T4',
          data: d[3].data,
          borderColor: '#40FF00',
        }, {
          label: 'T5',
          data: d[4].data,
          borderColor: '#565656',
        }

      ]
    }

    this.options = {
      title: {
        display: true,
        text: 'Historical Trend',
        fontSize: 16
      },
      legend: {
        position: 'top'
      }
    };
  }

  generatePresureChartDatat(d) {
   console.log(d[6].data)
    this.prData = {
      labels: d[6].data,
      datasets: [
        {
          label: 'PR',
          data: d[5].data,
          borderColor: '#0101DF',
        },
      ]
    }

    this.prOptions = {
      title: {
        display: true,
        text: 'Historical Trend',
        fontSize: 16
      },
      legend: {
        position: 'top'
      }
    };
  }


}
