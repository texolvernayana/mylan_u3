    export interface IhphvProcess {
        Equip_Id: string;
        Run_Id: string;
         Product_Name: string;
        User_Name: string;
         Load_Id: string;
        Starts_Date: string;
        Start_Time: string;
        Pre_Vaccum: string;
        Pre_Pressure: string;
        No_Of_Pre_Pulses: string;
        Pr_Pulse_Low_End_Point: string;
        Ster_Hold_Temp: string;
        Ster_Hold_Time: string;
        Ster_Overshoot: string;
        Ster_Stop_Temp: string;
        Ster_Reset_Temp: string;
        Post_Vaccum_Start_Pressure: string;
        Post_Vaccum: string;
        Post_Vaccum_Hold_Time: string;
        Post_Pressure: string;
        No_Of_Post_Pulses: string;
        Process_End_Pressure: string;
        Process_End_Temp: string;
        Pr_Pulse_High_Point: string;
        Pr_Pulse_Low_Point: string;
        No_Of_Pr_Pulses: string;
        Process_Name: string;
        Drying_Pressure: string;
        Drying_Hold_Time: string;
}
    
export class hphvProcess implements IhphvProcess{
    Equip_Id: string;    Run_Id: string;
    Product_Name: string;
    User_Name: string;
    Load_Id: string;
    Starts_Date: string;
    Start_Time: string;
    Pre_Vaccum: string;
    Pre_Pressure: string;
    No_Of_Pre_Pulses: string;
    Pr_Pulse_Low_End_Point: string;
    Ster_Hold_Temp: string;
    Ster_Hold_Time: string;
    Ster_Overshoot: string;
    Ster_Stop_Temp: string;
    Ster_Reset_Temp: string;
    Post_Vaccum_Start_Pressure: string;
    Post_Vaccum: string;
    Post_Vaccum_Hold_Time: string;
    Post_Pressure: string;
    No_Of_Post_Pulses: string;
    Process_End_Pressure: string;
    Process_End_Temp: string;
    Pr_Pulse_High_Point: string;
    Pr_Pulse_Low_Point: string;
    No_Of_Pr_Pulses: string;
    Process_Name: string;
    Drying_Pressure: string;
    Drying_Hold_Time: string;


}