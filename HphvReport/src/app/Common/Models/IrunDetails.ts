export interface iRunDetails {
    Log_Date: Date;
    Log_Time: string;
    FLT_TEMP: string;
    Run_Id: string;
    Phase: string;
    DR_T1: string;
    CH_T2: string;
    CH_T3: string;
    CH_T4: string;
    CH_T5: number;
    CH_Pr: string;
}