export interface Ibatch{
    Product_Name : string, 
    Load_Id : string, 
    Batch_no : string, 
    Print_Interval : string,
    Starts_Date : string,
    Start_Time : string
}

export class batch implements Ibatch {
    Product_Name: string;
    Load_Id: string;
    Batch_no: string;
    Print_Interval: string;
    Starts_Date: string;
    Start_Time: string;
}
