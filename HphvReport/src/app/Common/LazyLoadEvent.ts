import { FilterMetadata } from 'primeng/components/common/filtermetadata';

export interface LazyLoadEvent {
    first?: number;
    rows?: number;
    sortField?: string;
    sortOrder?: number;
    multiSortMeta?: SortMeta[];
    filters?: {[s: string]: FilterMetadata;};
    globalFilter?: any;
}

export interface FilterMetadata {
    value?: any;
    matchMode?: string;
}

export interface SortMeta {
    field: string;
    order: number;
}