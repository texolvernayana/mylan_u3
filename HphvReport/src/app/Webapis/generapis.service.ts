import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { IrunidLists } from '../Common/Models/runidList';


@Injectable({
  providedIn: 'root'
})
export class GenerapisService {
  private _url: string = "http://localhost:4201/api/hphvHederDetails";

  constructor(private http: HttpClient) { }

  getBatchDetails(batchNo: string): Observable<IrunidLists> {
    return this.http.get<IrunidLists>(this._url + "/runIdListWithRange?StartDateTime='2017-06-18 12:25:07.000'&StartDateTime='2017-07-18 12:25:07.000'")
    //return this.http.get(this.ProductAPI, {}).map(res => <Product[]>res.json());
  }
  
}
