import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { IwarningMessage, warningMessage } from '../Common/Models/IwarningMessages';

@Component({
  selector: 'app-parameters',
  templateUrl: './parameters.component.html',
  styleUrls: ['./parameters.component.css']
})
export class ParametersComponent implements OnInit {
  conatValues = {
    invalidFromDate : "Please select from date",
    invalidToDate : "Please select to date",
    invalidDateRange : "To date can not be less than From date",
}
  constructor(private router: Router, private route: ActivatedRoute) { }
  public dateStart: Date
  public dateEnd: Date
  public runId: string;
  warningmessage: IwarningMessage;

  ngOnInit() {
    this.warningmessage=new warningMessage();
  }

  searchRunId() {
    debugger;
    if (this.validateParameter() == true) {
      this.router.navigate(['/searchrunIdList'], { relativeTo: this.route })
    }
  }

  getFormatedDate(dateToBeFormated: Date) {
    return dateToBeFormated.getFullYear() + "-" + (dateToBeFormated.getMonth() + 1) + "-" + dateToBeFormated.getDate() + " " +
      dateToBeFormated.getHours() + ":" + dateToBeFormated.getMinutes() + ":" + dateToBeFormated.getSeconds();
  }

  validateParameter() {
    if (this.dateStart == undefined) {
      this.warningmessage.message = this.conatValues.invalidFromDate;
      this.warningmessage.cssClass = 'text-warning'
      this.warningmessage.isToDisplay = true;
      return false;
    }
    if (this.dateEnd == undefined) {
      this.warningmessage.message = this.conatValues.invalidToDate;
      this.warningmessage.cssClass = 'text-warning'
      this.warningmessage.isToDisplay = true;
      return false;
    }
    if (this.dateStart > this.dateEnd) {
      this.warningmessage.message = this.conatValues.invalidDateRange;
      this.warningmessage.cssClass = 'text-warning'
      this.warningmessage.isToDisplay = true;
      return false;
    }
    return true;
  }
}
