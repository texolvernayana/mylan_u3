import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { ParametersComponent } from './parameters.component';
import { SearchreunidsComponent } from './searchreunids/searchreunids.component';

const routes: Route[] = [
    {
        path: '',
        component: ParametersComponent,
        // children: [{
        //              path: 'searchrunIdList',  component: SearchreunidsComponent,
        // }]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);