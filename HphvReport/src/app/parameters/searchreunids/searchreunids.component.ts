import { Component, OnInit, Input } from '@angular/core';
import { IrunidLists } from '../../Common/Models/runidList';
import { GenerapisService } from 'src/app/Webapis/generapis.service';

@Component({
  selector: 'app-searchreunids',
  templateUrl: './searchreunids.component.html',
  styleUrls: ['./searchreunids.component.css']
})
export class SearchreunidsComponent implements OnInit {
  @Input() paramDateStart: Date;
  @Input() paramDateEnd: Date;
  @Input() paramRunID: string;
  public runIdList: IrunidLists;

  cols = [];

  brands: SelectItem[];

  colors: SelectItem[];

  yearFilter: number;

  yearTimeout: any;

  constructor(private api: GenerapisService) { }

  ngOnInit() {
    console.log(this.paramDateStart + " -- " + this.paramDateEnd + " -- " + this.paramRunID)
    this.loadDataToTable();
  }

  loadDataToTable() {

    this.api.getBatchDetails("4451").subscribe(s => {
      this.runIdList = JSON.parse(JSON.stringify(s));
    }, error => console.log(error));

    this.cols = [
      { field: 'DateAndTime', header: 'Date' },
      { field: 'DateAndTime', header: 'Time' },
      { field: 'Run_Id', header: 'RunId' },
    ];
  }

}
export interface SelectItem {
  label?: string;
  value: any;
  styleClass?: string;
  icon?: string;
  title?: string;
  disabled?: boolean;
}

/*this.runIdList = [
      { date: '12/02/2015', time: '11:85:10', runId: 1257 },
      { date: '12/02/2015', time: '13:86:00', runId: 2354 },
      { date: '12/02/2015', time: '12:87:00', runId: 3256 },
      { date: '12/02/2015', time: '19:86:00', runId: 8980 },

    ];
    this.cols = [
      { field: 'date', header: 'Date' },
      { field: 'time', header: 'Time' },
      { field: 'runId', header: 'RunId' },
    ];*/