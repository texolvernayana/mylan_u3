import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ParametersComponent } from './parameters.component';
import { routing } from './parameter.route';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        routing,
    ],
    declarations: [
        //SearchreunidsComponent,
        ParametersComponent,
    ],
    bootstrap: [
        ParametersComponent
    ]
})
export class ParametersModule { }
