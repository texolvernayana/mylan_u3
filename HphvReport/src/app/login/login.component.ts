import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute) { }
  //public alertStyle = "visibility: hidden";
  public hasLoginFailed: boolean;
  
  ngOnInit() {
    this.hasLoginFailed = false;
  }
  userDetails = {
    userName: '',
    password: ''
  }
  validateUser() {
  if (this.userDetails.userName === 'gg' && this.userDetails.password === "gg") {
      this.router.navigate(['/parameter'], { relativeTo: this.route })
      this.hasLoginFailed = false;
    }
    else {
      this.hasLoginFailed = true;
    }
  }

}
